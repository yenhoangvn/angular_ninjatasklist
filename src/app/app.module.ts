import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes, RoutesRecognized } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HTTP_INTERCEPTORS ,HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { NinjaTasksComponent } from './components/ninja-tasks/ninja-tasks.component';
import { NinjaTasksItemComponent } from './components/ninja-tasks-item/ninja-tasks-item.component';
import { AddNinjaTaskComponent } from './components/add-ninja-task/add-ninja-task.component';
import { AboutComponent } from './components/about/about.component';
import { HeaderTaskComponent } from './components/header-task/header-task.component';
import { ButtonHeaderTaskComponent } from './components/button-header-task/button-header-task.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

// import { LoginService } from './services/login.service';
// import { DataService } from './services/data.service';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { AuthGuard } from './guards/auth.guards';
import { SecureInnerPagesGuard } from './guards/secure-inner-pages.guards';




const appRoutes: Routes = [
  {path: '', component: NinjaTasksComponent},
  {path: 'about', component: AboutComponent},
  {path: 'login', component: LoginComponent, canActivate:[SecureInnerPagesGuard] },
  {path: 'register', component: RegisterComponent, canActivate:[SecureInnerPagesGuard] },
  {path: 'logout', component: DashboardComponent, canActivate:[AuthGuard]}

]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    NinjaTasksComponent,
    NinjaTasksItemComponent,
    AddNinjaTaskComponent,
    AboutComponent,
    HeaderTaskComponent,
    ButtonHeaderTaskComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes,{enableTracing:true})
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }, AuthGuard, SecureInnerPagesGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
