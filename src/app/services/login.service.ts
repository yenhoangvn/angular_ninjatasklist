import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../User';
import { Observable, of, pipe } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

// const httpOptions = {
//   headers: new HttpHeaders({
//     'content-Type': 'application/json'
//   })
// }
const noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };
const AuthHeader = { headers: new HttpHeaders() };
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  token:any;
  username:any;
  isLoggedIn: boolean = false;

  private apiUrl = 'https://whispering-everglades-72204.herokuapp.com/users/'


  constructor(private http:HttpClient) { }

  register(user: User): Observable <User>{
    console.log('user ====>',user)
    return this.http.post<User>(`${this.apiUrl}register`, user, noAuthHeader)
  }

  login(user: User): Observable <User>{
    console.log('user ====>',user)
    return this.http.post<User>(`${this.apiUrl}login`, user, noAuthHeader)
  }
  storeUserData(token:any, username:any, userId:any){
    localStorage.setItem('id_token',token)
    localStorage.setItem('username',username)
    localStorage.setItem('userId',userId)
    this.token = token;
    this.username = username;
    // console.log('token in client',token)
  }
  verify_token(): Observable <any>{
    return this.http.post<any>(`${this.apiUrl}verify_token`,AuthHeader).pipe(
      tap((res:any)=>{
        console.log('res here===>',res)
        if(res.ok){
          this.isLoggedIn = true;
          console.log('check here after verify===>',this.isLoggedIn)
        }else{
          this.isLoggedIn = false
        }
      })
    )
  }
  // loggedIn(): boolean{
  //   return this.isLoggedIn;
  // }
  loggedIn(): boolean {
    let authToken = localStorage.getItem('id_token');
    console.log('token ===>',authToken)
    return (authToken !== null) ? true : false;
  }
  
  logout(){
    localStorage.removeItem('id_token');
    localStorage.removeItem('userId');
    localStorage.removeItem('username');
    this.isLoggedIn = false;
    localStorage.clear()
  }
  
}
