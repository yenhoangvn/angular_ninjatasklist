import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  private isLoggedIn: any;
  private userId: any;
  private subject = new Subject<any>();


  constructor() { }

  sendData( data:any ): void{
    this.isLoggedIn = data;
    console.log('data ==>',data)
    this.subject.next(this.isLoggedIn)
  }
  getData(): Observable<any>{
    return this.subject.asObservable();
  }
  // getData(): Observable<any> {
  //   return this.subject.asObservable();
  // }
  showData(){
    console.log(this.isLoggedIn)
    return this.isLoggedIn
  }
}
