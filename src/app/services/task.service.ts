import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Task } from '../Task';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'content-Type': 'application/json', 
  })
}

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  token:any;
  private apiUrl = 'https://whispering-everglades-72204.herokuapp.com/tasks/'

  constructor(private http:HttpClient) { }

  getTasks(): Observable <Task[]> {
    return this.http.get <Task[]>(this.apiUrl);
  }
  deleteTask(task: Task): Observable <Task> {
    const url = `${this.apiUrl}delete/${task._id}`
    console.log(url)
    return this.http.delete<Task>(url);
  }
  updateTaskReminder(task: Task): Observable <Task>{
    const url = `${this.apiUrl}update/${task._id}`
    return this.http.post<Task>(url, task, httpOptions)
  }
  addTask(task: Task): Observable <Task>{
    console.log('task input ====>',task)
    return this.http.post<Task>(`${this.apiUrl}add`, task, httpOptions)
  }
}

