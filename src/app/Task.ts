export interface Task {
    _id?: string;
    userId: any;
    text: string;
    day: string;
    time: string;
    reminder: boolean;

}