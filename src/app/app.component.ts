import { Component } from '@angular/core';
import { LoginService } from './services/login.service';
// import { DataService } from './services/data.service';
// import { Subscription } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'NinjaTasks';
  // isLoggedIn: boolean = false;
  // subscription: Subscription;

  constructor( private loginService: LoginService ) {
    // this.subscription = this.ds
    //   .getData()
    //   .subscribe((value) => (this.isLoggedIn = value)); 
  }
  
  ngOnInit(): void {
    this.loginService.verify_token().subscribe(
      // (res:any)=>{
      //   console.log('res from verify token',res);
      //   if(res.ok){
      //     this.isLoggedIn = true;
      //     this.ds.sendData(this.isLoggedIn);
      //     console.log('check ====>',this.isLoggedIn)
      //   }else{
      //     this.isLoggedIn = false;
      //     this.ds.sendData(this.isLoggedIn);
      //   }
      // }
    )
    
  }
  

}
