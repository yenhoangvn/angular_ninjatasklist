import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UiService } from '../../services/ui.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-header-task',
  templateUrl: './header-task.component.html',
  styleUrls: ['./header-task.component.css']
})
export class HeaderTaskComponent implements OnInit {
  username!: any;
  title: string = 'Task Tracker';
  showAddTask: boolean = true;
  subscription!: Subscription;
  constructor(private uiService: UiService, public loginService: LoginService ) {
    this.subscription = this.uiService
      .onToggle()
      .subscribe((value) => (this.showAddTask = value));
  }

  ngOnInit(): void {
    this.username = localStorage.getItem('username')
  }
  toggleAddTask() {
    this.uiService.toggleAddTask();
  }
}
