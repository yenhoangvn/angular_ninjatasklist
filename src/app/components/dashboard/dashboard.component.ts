import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
 

  constructor( private loginService:LoginService, private router: Router ) { }

  ngOnInit(): void {
    this.loginService.verify_token().subscribe()
  }
  onClick(){
    this.loginService.logout();
    this.router.navigate(['/login']);

  }
}
