import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Task } from '../../Task';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-ninja-tasks-item',
  templateUrl: './ninja-tasks-item.component.html',
  styleUrls: ['./ninja-tasks-item.component.css']
})
export class NinjaTasksItemComponent implements OnInit {
  @Input() task!: Task
  faTimes = faTimes 
  @Output() onDeleteTask: EventEmitter<Task> = new EventEmitter()
  @Output() onToggleReminder: EventEmitter<Task> = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
  }
  onDelete(task:any){
    this.onDeleteTask.emit(task);
  }
  onToggle(task:any){
  this.onToggleReminder.emit(task)
  }

}
