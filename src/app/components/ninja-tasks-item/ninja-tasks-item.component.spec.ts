import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NinjaTasksItemComponent } from './ninja-tasks-item.component';

describe('NinjaTasksItemComponent', () => {
  let component: NinjaTasksItemComponent;
  let fixture: ComponentFixture<NinjaTasksItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NinjaTasksItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NinjaTasksItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
