import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  username!: string;
  email!: string;
  password!: string;
  password2!: string;
  message!: string;

  constructor(
    private loginService: LoginService,
    private router: Router
    ) { }
  
  ngOnInit(): void {
  }
  onSubmit(){

    const user: any = {
      username: this.username,
      email: this.email,
      password: this.password,
      password2: this.password2,
    };
    
    this.loginService.register(user).subscribe( 
      (res:any) => {
        if(res.ok){
          console.log('res',res);
          this.router.navigate(['/login'])
        }else{
          this.message = res.message;
          return;
        }
      },
      err =>{
        console.log(err)
      }
    )

    this.username = '';
    this.email = '';
    this.password= '';
    this.password2 = '';
  }

}
