import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button-header-task',
  templateUrl: './button-header-task.component.html',
  styleUrls: ['./button-header-task.component.css']
})
export class ButtonHeaderTaskComponent implements OnInit {
  @Input() text!: string;
  @Input() color!: string;
  @Output() btnClick = new EventEmitter()
  constructor() { }

  ngOnInit(): void {
  }
  onClick(){
    this.btnClick.emit()
  }
}
