import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonHeaderTaskComponent } from './button-header-task.component';

describe('ButtonHeaderTaskComponent', () => {
  let component: ButtonHeaderTaskComponent;
  let fixture: ComponentFixture<ButtonHeaderTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonHeaderTaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonHeaderTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
