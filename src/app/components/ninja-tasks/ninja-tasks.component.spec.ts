import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NinjaTasksComponent } from './ninja-tasks.component';

describe('NinjaTasksComponent', () => {
  let component: NinjaTasksComponent;
  let fixture: ComponentFixture<NinjaTasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NinjaTasksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NinjaTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
