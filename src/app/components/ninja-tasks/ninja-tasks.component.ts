import { Component, OnInit } from '@angular/core';
import { Task } from '../../Task';
import { TaskService } from '../../services/task.service';
import { LoginService } from '../../services/login.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ninja-tasks',
  templateUrl: './ninja-tasks.component.html',
  styleUrls: ['./ninja-tasks.component.css']
})
export class NinjaTasksComponent implements OnInit {
  tasks: Task[] = [];
  isLoggedIn!: boolean;
  subscription!: Subscription;
  
  constructor( private taskService: TaskService, private loginService: LoginService ) {
   }

  ngOnInit(): void {
    this.taskService.getTasks().subscribe((tasks) => {
      const userId = localStorage.getItem('userId')
      this.tasks = tasks.filter(ele=>ele.userId === userId)
      console.log(this.tasks)
    });
    this.loginService.verify_token().subscribe();
  }
  deleteTask(task: Task) {
    this.taskService
      .deleteTask(task)
      .subscribe(
        () => (this.tasks = this.tasks.filter((t) => t._id !== task._id))
      );
  }
  toggleReminder(task: Task){
    task.reminder = !task.reminder
    this.taskService.updateTaskReminder(task).subscribe()
  }
  addTask(task: Task){
    this.taskService.addTask(task).subscribe((task) => (this.tasks.push(task)));
  }


}
