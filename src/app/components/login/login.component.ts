import { Component, OnInit } from '@angular/core';
import { User } from '../../User';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  email!: string;
  password!: string;
  message!: string;

  constructor( private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    
  }
  onSubmit(){

    const user: any = {
      email: this.email,
      password: this.password
    };
    this.loginService.login(user).subscribe( 
      (res:any) => {
      console.log('res',res);
      this.message = res.message;
      if(res.ok){
        this.loginService.storeUserData(res.token, res.username, res.userId);
        this.loginService.verify_token().subscribe();
        this.router.navigate(['/']);
      }else{
        this.message = res.message;
        return;
      }
      
      },
      err =>{
        console.log(err)
      }
    )
    
    this.email = '';
    this.password= '';
  }
}
