import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNinjaTaskComponent } from './add-ninja-task.component';

describe('AddNinjaTaskComponent', () => {
  let component: AddNinjaTaskComponent;
  let fixture: ComponentFixture<AddNinjaTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNinjaTaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNinjaTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
