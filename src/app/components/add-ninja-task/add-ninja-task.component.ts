import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Task } from '../../Task';
import { UiService } from '../../services/ui.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-add-ninja-task',
  templateUrl: './add-ninja-task.component.html',
  styleUrls: ['./add-ninja-task.component.css']
})
export class AddNinjaTaskComponent implements OnInit {
  @Output() onAddTask: EventEmitter<Task> = new EventEmitter();
  text!: string;
  day!: string;
  time!: string;
  reminder: boolean = false;
  showAddTask: boolean = true;
  subscription!: Subscription;

  constructor(private uiService: UiService) {
    this.subscription = this.uiService
      .onToggle()
      .subscribe((value) => (this.showAddTask = value));
  }

  ngOnInit(): void {
  }
  onSubmit(){
    const userId = localStorage.getItem('userId');
    if(!userId){
      alert('please logIn to continue')
    }
    if (!this.text) {
      alert('please add a task!');
      return;
    }
    const newTask: any = {
      userId: userId,
      text: this.text,
      day: this.day,
      time: this.time,
      reminder: this.reminder,
    };
    this.onAddTask.emit(newTask);

    this.text = '';
    this.day = '';
    this.time= '';
    this.reminder = false;
  }

}
