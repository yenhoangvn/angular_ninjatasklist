import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private loginService : LoginService, private router : Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if (!this.loginService.loggedIn()) {
        console.log('auth guard check ===>', this.loginService.loggedIn())
        window.alert('Access Denied, Login is Required to Access This Page!')
        this.router.navigateByUrl('/login');
        return false;
      }
    return true;
  }
}